use super::Name;
use bevy::{prelude::*, tasks::prelude::*};

pub struct MovementPlugin;

impl Plugin for MovementPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(add_shit.system())
            .add_system(orbit_system.system());
    }
}

#[derive(Debug)]
pub struct Orbit {
    kps: f32,
    radius: f32,
    radian: f32,
}

impl Orbit {
    pub fn new(kps: f32, radius: f32) -> Self {
        Orbit {
            kps,
            radius,
            radian: 0.0,
        }
    }
    fn next(&mut self, delta: f32) {
        self.radian += ((self.kps * delta) % self.radius) / self.radius;
    }

    pub fn get_x(&self) -> f32 {
        self.radian.cos() * self.radius
    }

    pub fn get_y(&self) -> f32 {
        self.radian.sin() * self.radius
    }
}

fn orbit_system(
    time: Res<Time>,
    pool: Res<ComputeTaskPool>,
    mut orbits: Query<(&mut Orbit, &mut Transform)>,
) {
    orbits
        .iter()
        .par_iter(16)
        .for_each(&pool, |(mut orbit, mut transform)| {
            orbit.next(time.delta_seconds);
            let mut translation = transform.translation_mut();

            translation.set_x(orbit.get_x());
            translation.set_y(orbit.get_y());
        });
}

fn add_shit(mut commands: Commands) {
    commands.spawn((
        Orbit::new(100.0, 100.0),
        Name("Test".to_string()),
        Transform::default(),
    ));
}
