use super::movement::Orbit;
use bevy::prelude::*;
use rand::Rng;
pub struct SystemPlugin;

impl Plugin for SystemPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system());
    }
}

#[derive(Clone, Copy)]
pub enum BodyType {
    Planet,
    Moon,
    Asteroid,
    Star,
    JumpPoint,
}

impl BodyType {
    fn get_sprite_components(
        &self,
        materials: &mut ResMut<Assets<ColorMaterial>>,
    ) -> SpriteComponents {
        match self {
            Self::Star => SpriteComponents {
                material: materials.add(YELLOW.into()),
                sprite: Sprite::new(Vec2::new(30.0, 30.0)),

                ..Default::default()
            },
            Self::Asteroid => SpriteComponents {
                material: materials.add(Color::BLACK.into()),
                sprite: Sprite::new(Vec2::new(3.0, 3.0)),
                ..Default::default()
            },
            _ => unimplemented!(),
        }
    }
}

fn setup(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    _asset_server: Res<AssetServer>,
) {
    println!("Starting System Setup");
    create_body(&mut commands, &mut materials, BodyType::Star);
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        create_body(&mut commands, &mut materials, BodyType::Asteroid);
        let r = rng.gen_range(50.0, 500.0);

        commands.with(Orbit::new(50.0, r));
    }
}

fn create_body(
    commands: &mut Commands,
    mut materials: &mut ResMut<Assets<ColorMaterial>>,
    bt: BodyType,
) {
    commands
        .spawn(bt.get_sprite_components(&mut materials))
        .with(bt);
}

pub fn create_sprite(
    color: Color,
    materials: &mut ResMut<Assets<ColorMaterial>>,
) -> SpriteComponents {
    SpriteComponents {
        material: materials.add(color.into()),
        sprite: Sprite::new(Vec2::new(3.0, 3.0)),
        ..Default::default()
    }
}

const YELLOW: Color = Color::rgb(255.0, 255.0, 0.0);
