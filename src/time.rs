use bevy::prelude::*;

pub struct TimePlugin;

impl Plugin for TimePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_resource(GameTime::default())
            .add_system(update_game_time.system());
    }
}

#[derive(Default)]
pub struct GameTime {
    subsecond: f32,
    time: i64,
}

impl GameTime {
    fn update(&mut self, delta_seconds: f32) {
        self.subsecond += delta_seconds;
        if self.subsecond >= 1.0 {
            self.time += self.subsecond.trunc() as i64;
            self.subsecond = self.subsecond.fract();
        }
    }

    pub fn format(&self) -> String {
        use chrono::prelude::*;
        let naive = NaiveDateTime::from_timestamp(self.time, 0);
        let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
        datetime.format("%Y-%B-%d %H:%M:%S").to_string()
    }
}

fn update_game_time(time: Res<Time>, mut game_time: ResMut<GameTime>) {
    game_time.update(time.delta_seconds);
}
