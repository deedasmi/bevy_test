#![allow(dead_code)]
use bevy::diagnostic::*;
use bevy::prelude::*;

mod movement;
mod system;
mod time;

use self::movement::MovementPlugin;
use self::system::SystemPlugin;
use self::time::TimePlugin;

pub struct Name(String);

fn main() {
    App::build()
        .add_default_plugins()
        .add_startup_system(game_setup.system())
        .add_plugin(TimePlugin)
        .add_plugin(SystemPlugin)
        .add_plugin(MovementPlugin)
        .add_plugin(FrameTimeDiagnosticsPlugin)
        .add_plugin(PrintDiagnosticsPlugin {
            debug: false,
            wait_duration: std::time::Duration::new(1, 0),
            filter: None,
        })
        .run();
    println!("App started!");
}

fn game_setup(mut commands: Commands) {
    commands.spawn(Camera2dComponents::default());
}
